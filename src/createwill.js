import React, { Component } from 'react';
import {Button, Input} from 'react-materialize';
import {Row, Col} from 'reactstrap';

import EosjsEcc from 'eosjs-ecc';

class CreateWill extends Component {

  onSubmit(e) {
    e.preventDefault();
    console.log('EosjsEcc', EosjsEcc);
    console.log('submit');
    let message = this.refs.inpMessage.value;
    let data = {
      title: this.refs.inpTitle.state.value || 'No title',
      recipient: this.refs.inpRecipient.state.value,
      period: Number(this.refs.inpPeriod.state.value) || 10,
    }
    if (!data.recipient) return;
    this.setState({
      data
    });
    EosjsEcc.randomKey().then(pkey => {
      console.log('pkey', pkey);
      //data.signerPrivateKey = pkey;
      let encPkey = EosjsEcc.Aes.encrypt(pkey, data.recipient, pkey);
      data.signerPrivateKey = [
        encPkey.nonce.toString(),
        encPkey.message.toString('hex'),
        encPkey.checksum
      ].join(';');
      data.signerPublicKey = EosjsEcc.privateToPublic(pkey);
      let encryptedMessage = EosjsEcc.Aes.encrypt(pkey, data.signerPublicKey, message);
      data.nonce = encryptedMessage.nonce.toString();
      data.message = encryptedMessage.message.toString('hex');
      data.checksum = encryptedMessage.checksum;
      //console.log('encrypted', data.encryptedMessage, typeof data.encryptedMessage.message);
      console.log('encrypted', JSON.stringify(data));

      console.log('msg', data.message);
 
      this.setState({
        data
      });
      if (this.props.onCreate) {
        this.props.onCreate(data);
      }
    });
  }

  render() {
  
    return (
    
      <form  className="form" onSubmit={this.onSubmit.bind(this)}>

        <h2>Create new message</h2>

        <Row className="form-group">
          <Col className="form-group">
            <label htmlFor="inp-title" >Title</label>
            <Input type="text" id="inp-title" name="title" ref="inpTitle" className="form-control" />
          </Col>
        </Row>

        <Row className="form-group">
          <Col className="form-group">
            <label htmlFor="inp-title" >Secret message</label>
            <textarea id="inp-message" name="message" ref="inpMessage" className="form-control" rows="10" />
          </Col>
        </Row>

        <Row className="form-group">
          <Col className="form-group">
            <label htmlFor="inp-title" >Recipient EOS address</label>
            <Input type="text" id="inp-recipient" name="recipient" ref="inpRecipient" className="form-control" />
          </Col>
        </Row>

        <Row className="form-group">
          <Col className="form-group">
            <label htmlFor="inp-title" >Idle time in days (seconds for demo)</label>
            <Input type="text" id="inp-period" name="period" ref="inpPeriod" className="form-control" />
          </Col>
        </Row>

        <div className="form-footer">
         <Button to="/deployment" className="btn btn-primary" >Create</Button>
        </div>
      </form>
    );
  }
}

export default CreateWill;
