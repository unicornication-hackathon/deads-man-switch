import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import {Container} from 'reactstrap' 
import Login from './login';
import WillList from './willlist';
import CreateWill from './createwill';
import api from './eosapi';

//import Eosjs from 'eosjs';

class App extends Component { 

  constructor() {
    super();
    this.state = {
      publicKey: ''
    }




  }

  onLogin(publicKey) {
    console.log('set pkey ', publicKey);
    this.setState({
      publicKey
    });
    //this.forceUpdate();
  }

  onCreateMessage(data) {
    api.addMessage(data).then(res => {
    
    }).catch(console.error);
  }

  render() {
    console.log(this.state);
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Dead man's switch</h1>
        </header>
        <Container className="pb-3">
          {this.state.publicKey.length === 0 &&
           <Login onLogin={this.onLogin.bind(this)}/>
          }
          {this.state.publicKey.length > 0 &&
            <section>
              <WillList publicKey={this.state.publicKey} />
            </section>
          }
        </Container>
        <Container>
          <CreateWill onCreate={this.onCreateMessage.bind(this)}/>
        </Container>
      </div>
    );
  }
}

export default App;
