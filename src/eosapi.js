
//import request from 'request';
//import Eos from 'eosjs';
//import EosjsEcc from 'eosjs-ecc';

const defaultOptions = { 
  //url: 'http://10.101.1.10:8888/v1/',
  url: 'http://10.101.1.4:3000/',
  method: 'POST',
  json: true,
  body: {},
};


class EosApi {

  request(opts) {
    return new Promise((resolve, reject) => {

      /*
      console.log('request', opts);
      request(opts, function (error, response, body) {
        if (error) return reject(error);
        return resolve(body);
      });
      */
      var xhr = new XMLHttpRequest();
      xhr.open(opts.method, opts.url, true);
      xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
      xhr.onload = () => {
        let res = xhr.responseText;
        console.log('result', res);
        return resolve(res);
      };
      let r = xhr.send(JSON.stringify(opts.body));
      console.log('send ', r);
    });
  }

  addMessage(data) {
    let opts = {...defaultOptions};
    opts.url += 'message';
    //data.encryptedMessage = JSON.stringify(data.encryptedMessage);
    let t = Date.now();
    console.log('T', t, (new Date(t)).toLocaleString());
    t = Math.ceil(t / 1000);
    console.log('T', t, (new Date(t)).toLocaleString());
    t = Number(t) + Number(data.period || 10);
    console.log('T', t, (new Date(t)).toLocaleString());
    data.tillTime = t;
    opts.body = data;
    return this.request(opts);
  }

  loadMessages(pubKey) {
    let opts = {...defaultOptions};
    opts.url += 'message/'+pubKey;
    opts.method = 'GET'
    return this.request(opts);
  }

  getKeys(pubKey) {
    let opts = {...defaultOptions};
    opts.url += 'private_keys/'+pubKey;
    opts.method = 'GET'
    return this.request(opts);
    
  }



  
}

export default new EosApi();
