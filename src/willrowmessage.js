import React, { Component } from 'react';
import {Button} from 'react-materialize';
import EosjsEcc from 'eosjs-ecc';


class WillRowMessage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      decrypted: false,
      message: ''
        
    }
  }

  onSubmit(e) {
    e.preventDefault();
    let myPkey = this.refs.pkeyInput.value;
    //if (!pkey.length) return ;
    let encPkey = this.props.privateKey;
    if (!myPkey.length) return ;
    console.log('pkey', myPkey)

    let [nonce, msg, checksum] = encPkey.split(';');
    let signPkey = EosjsEcc.Aes.decrypt(
      myPkey,
      this.props.signerPublicKey,
      nonce,
      Buffer.from(msg, 'hex'),
      Number(checksum)
    ).toString();
    console.log('decrypted signPkey', signPkey);

    /*
      console.log(
          'EosjsEcc.Aes.decrypt(',
        pkey, 
        this.props.signerPublicKey,
        this.props.encryptedMessage.nonce, 
        this.props.encryptedMessage.message, 
        this.props.encryptedMessage.checksum

          )
          */
    console.log('decrypt', this.props);
    let message = EosjsEcc.Aes.decrypt(
        signPkey, 
        this.props.signerPublicKey,
        this.props.nonce, 
        Buffer.from(this.props.message, 'hex'),
        Number(this.props.checksum));
    console.log('decrypted', message);
    this.setState({
      decrypted: true,
      message: message.toString()
    });
  }

  render() {
  
    let now = Date.now();
    if (now < this.props.tillTime) return (
      <tr>
        <td colSpan="3">
          Not available yet
        </td>
      </tr>
    );
    else if (!this.state.decrypted) {
   

     
      return (
        <tr>
          <td colSpan="3">
            <form className="form-inline form-horizontal" onSubmit={this.onSubmit.bind(this)}>
              <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                  <label >To decrypt message enter your private key
                    <input type="text" name="pk" ref="pkeyInput" />
                  </label>
                </div>
              </div>
              <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                  <Button className="btn btn-primary" disabled={this.props.privateKey ? false : true} >Decode message</Button>
                </div>
              </div>
            </form>
          </td>
        </tr>
      )
    } else {
    
      return (
        <tr>
          <td colSpan="3">{this.state.message}</td>
        </tr>
      )
    }


  }
}

export default WillRowMessage; 
