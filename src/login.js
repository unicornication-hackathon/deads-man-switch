import React, { Component } from 'react';
//import ReactDOM from 'react-dom';
import {Button, Input} from 'react-materialize';
import {Row, Col} from 'reactstrap' 

class Login extends Component {


  onLogin(e) {
    e.preventDefault();
    console.log('fld', this.refs.loginInput);
    let pkey = this.refs.loginInput.state.value;
    console.log({pkey});
    if (this.props.onLogin) this.props.onLogin(pkey);
  }
  render() {
    return (
      <form className="form" onSubmit={this.onLogin.bind(this)}>
        <h2>Enter your public key to see messages for you</h2>
        <Row>
          <Col className="form-group">
            <Input name="publicKey" className="form-control" type="text" defaultValue="" ref="loginInput" />
          </Col>

        </Row>
        <div className="form-footer">
         <Button className="btn btn-primary" >Log in</Button>
        </div>
      </form>
    );
  }
}

export default Login;
