import React, { Component } from 'react';
import WillRowMessage from './willrowmessage';
//import EosjsEcc from 'eosjs-ecc';

import api from './eosapi';


class WillList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: []
    }
    //console.log({publicKey});
    api.loadMessages(this.props.publicKey).then(mess => {
      try {
        mess = JSON.parse(mess);
      } catch(e) {
        mess = [];
        console.error(e);
      }
      mess.map((m, ind) => {
        
        console.log('mess', m);
        mess[ind].tillTime = Number(m.tillTime) * 1000;
        console.log('T', mess[ind].tillTime, (new Date(mess[ind].tillTime)).toLocaleString());
        return '';
      })
      //mess.encryptedMessage = JSON.parse(mess.encryptedMessage);
      console.log('messages', mess);
      this.setState({
        messages: mess
      });
    });

    api.getKeys(this.props.publicKey).then(pkeys => {
      console.log('pkeys', pkeys);
      try {
        pkeys = JSON.parse(pkeys);
      } catch(e) {
        pkeys = [];
        console.error(e);
      }
      let keyObj = {};
      pkeys.map(({signerPrivateKey, signerPublicKey}) => {
        keyObj[signerPublicKey] = signerPrivateKey;
        return '';
      });
      this.setState({
        privateKeys: keyObj
      });
    });
  }

  render() {

    const WillRowInfo = (props) => {
      let d = new Date(props.tillTime);
      return (
        <tr>
          <td>{props.title}</td>
          <td>{props.signerPublicKey}</td>
          <td>{d.toLocaleString()}</td>
        </tr>
      )
    }

    let i = 0;
    return (
      <section>
        <h2>Messages for you</h2>
        <table className="table"> 
          <thead>
            <tr>
              <td>Title</td>
              <td>Signer public key</td>
              <td>Locked until</td>

            </tr>
          </thead>
          <tbody>
            {this.state.messages.map(row => {

              let pkey = this.state.privateKeys && this.state.privateKeys[row.signerPublicKey]
                ? this.state.privateKeys[row.signerPublicKey]
                : '';
              return (
               [
                <WillRowInfo key={i++} {...row} />,
                <WillRowMessage key={i++} {...row} privateKey={pkey} />
               ]
              );

            })}
          </tbody>
        </table>
      </section>
    );
  }
}

export default WillList;
