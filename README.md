# Dead Man's Switch

*An EOS dapp to never lose your assets*

This project has been developed from scratch at #EOSHACKATHON on June 9-10, 2018.

**Overview**

The dapp publishes an encrypted container to the EOS blokchain.

The private key to decrypt the container is sent to the recovery account.

The user that publishes the container configures the dapp to send the private key to the recovery account if the user is idle for a certain period of time.

**Architecture**

There are three accounts that operate the Dead Man's Switch:

1. Main user
2. Virtual mediator
3. Recovery user

The main user creates a text object and puts any information into it. For example, public-private key pairs from any blockchain.

The main user assigns a recovery user by specifying their account and public key.

The main user sets their idle period in days or configures the dapp trigger on the idle time when there were no outgoing transactions.

The dapp creates an account virtual mediator public-private key pairs.

By using the main user's private key and the virtual mediator's public key, the dapp turns the text object into an encrypted container.

The dapp deploys the standard smart contract that issues int( length( mainEncryptedContainer ) / 250 ) + 2 tokens with a unique name — "DMS_" + uniqueID.

The dapp turns the container into a string array that consists of lines 250 characters each.

The dapp transfers the int( length( mainEncryptedContainer ) / 250 ) + 1 issued token from the main user account to the virtual mediator account.

The dapp transfers the last token from the main user account to the recovery user account.

The encrypted container is immutable on the blockchain.

By using the private key of the virtual mediator and the recovery user, the dapp encrypts and puts the private key of the virtual mediator in the container. The container with the key can only be decrypted with the private key of the Recovery user.

The dapp deploys the smart contract. The smart contract has a delayed transaction of one token from the virtual mediator account to the Recovery user account and puts the container in the context_free_data field of the transaction.

When there is an outgoing transaction on the main user account, the delayed transaction is canceled and a new delayed transaction is created. 

## Install
```
  npm install
```

## Develop
```
  npm start
```
go to http://localhost:3000

## Build 
```
  npm build
```

